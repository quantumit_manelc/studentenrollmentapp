# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
A Technical coding exam that creates/updates a Class and Student
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

### Configuration ###
*Attach the database StudentEnrollDb from ~\StudentEnrollmentApp\Sea.Core\Database folder.
*Change the connection string from App.config of Sea.Core project. 
*Change the data source to the source of the local machine where you attach the database. 
*Change the user id and password. Change also the connection string from Web.config file of Sea.Web project.

###How to run tests###
*Click the Test from the Menu tools and click Run then Run All Tests

###To access the Rest API endpoint###
http://localhost:portnumber/OData/ClassApi?$expand=Students